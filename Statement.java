import java.util.*;

// Class for object of formula converted from encoded string array
public class Statement {

	// Class for object of one parentheses {} in the input statement
	public static class Clause {
		ArrayList<String> literals;
		public Clause(ArrayList<String> literals) {
			this.literals = literals;
		}
	}

	ArrayList<Clause> clauses;

	public Statement(String[][] statement) {
		ArrayList<Clause> clauses = new ArrayList<>();
		for (int i = 0; i < statement.length; i++) {
			String[] clause = statement[i];
			clauses.add(new Clause(new ArrayList<>(Arrays.asList(clause))));
		}
		this.clauses = clauses;
	}

	public Statement(ArrayList<Clause> clauses) {
		this.clauses = clauses;
	}

	public void setValueForLiteral(String literal, boolean value) {
		if (value) {
			removeClausesWithLiteral(literal);
			removeLiteralFromAllClauses("-" + literal);
		} else {
			removeClausesWithLiteral("-" + literal);
			removeLiteralFromAllClauses(literal);
		}
	}

	public boolean containsLiteral(String literal) {
		for (Clause clause : clauses) if (clause.literals.contains(literal)) 	return true;
		return false;
	}

	public boolean isAllClausesEmpty() {
		for (Clause clause : clauses)	if (clause.literals.size() != 0)	return false;
		return true;
	}

	public void removeLiteralFromAllClauses(String literal) {
		for (Clause clause : clauses)	if (clause.literals.contains(literal))	clause.literals.remove(literal);
	}

	public void removeClausesWithLiteral(String literal) {
		for (Iterator<Clause> iterator = clauses.iterator(); iterator.hasNext(); ) {
			Clause clause = iterator.next();
			if (clause.literals.contains(literal)) 	iterator.remove();
		}
	}

	public ArrayList<String> getLiterals() {
		LinkedHashSet<String> set = new LinkedHashSet<>();
		for (Clause clause : clauses)
			for (String literal : clause.literals)
				set.add(literal.startsWith("-") ? literal.replaceFirst("-", "") : literal);
		ArrayList<String> literals = new ArrayList<>(set);
		Collections.sort(literals);
		return literals;
	}

	public static Statement copyObject(Statement statement) {
		ArrayList<Clause> newObject = new ArrayList<>();
		for (Clause clause : statement.clauses) {
			ArrayList<String> literals = new ArrayList<>();
			for (String lit : clause.literals) {
				literals.add(new String(lit));
			}
			newObject.add(new Clause(literals));
		}
		return new Statement(newObject);
	}

}

