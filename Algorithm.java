import java.util.ArrayList;

public class Algorithm {

	public static void main(String[] args) {
		Algorithm result = Algorithm.checkSat(new Statement(new String[][] {{"p", "-q"}, {"-p", "q", "r"}, {"-p"}}));
		if(result != null) {
			System.out.println("SAT");
		} else {
			System.out.println("UNSAT");
		}
	}

	static Algorithm checkSat(Statement statement) {

		if (statement.clauses.size() == 0) return new Algorithm();
		if (statement.isAllClausesEmpty()) return null;
		ArrayList<String> literals = statement.getLiterals();

		String decision = "none";

		String literal = null;
		OneLiteralRuleLoop:
		for (Statement.Clause clause : statement.clauses)
			if (clause.literals.size() == 1) {
				literal = clause.literals.get(0);
				if (literal.startsWith("-")) {
					literal = literal.replaceFirst("-", "");
					decision = "right";
				} else {
					decision = "left";
				}
				break OneLiteralRuleLoop;
			}

		if (decision == "none")
			PureLiteralLoop:
					for (Statement.Clause c : statement.clauses)
						for (String l : c.literals) {
							String opposite = l.startsWith("-") ? l.replaceFirst("-", "") : "-" + l;
							if (!statement.containsLiteral(opposite))
								if (l.startsWith("-")) {
									literal = l.replaceFirst("-", "");
									decision = "right";
								} else {
									literal = l;
									decision = "left";
								}
							break PureLiteralLoop;
						}

		if (literal == null) literal = literals.get(0);

		if (decision != "right") {
			Statement newLeftStatement = Statement.copyObject(statement);
			newLeftStatement.setValueForLiteral(literal, true);
			Algorithm LeftLeaf = checkSat(newLeftStatement);
			if (LeftLeaf != null) return LeftLeaf;
		}

		if (decision != "left") {
			Statement newRightStatement = Statement.copyObject(statement);
			newRightStatement.setValueForLiteral(literal, false);
			Algorithm RightLeaf = checkSat(newRightStatement);
			if (RightLeaf != null) return RightLeaf;
		}
		return null;
	}
}

